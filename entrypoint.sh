#!/bin/sh

# if any line fails, return failure
set -e  

# pass .ENV variables from template to conf file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# start nginx Docker service
nginx -g 'daemon off;'