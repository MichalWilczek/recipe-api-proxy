server {
    listen ${LISTEN_PORT};
    large_client_header_buffers 4 64k;
    error_log error.log info;

    location /static {
        alias /vol/static;
    }

    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;
    }
}
