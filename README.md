# Recipe API Proxy

NGINGX proxy for our recipe app API.

The code is based on the course of Mark Winterbottom offered on [Udemy](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/).

## Usage

## Environment Variables

* 'LISTEN_PORT' - Port to listen (on default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forward requests to (default: '9000')
